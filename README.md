## Unsplash Randomizer

Script to download random images from our beloved Unsplash.

Install dependencies
```
 cd folder-of-the-script
 npm install
```

Run the script
```
 node index.js
```