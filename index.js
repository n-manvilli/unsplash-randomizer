// Include NPM packages
var axios = require('axios');
var wget = require('node-wget');

// REST request information
var req = {
    apiUser: 'n.manvilli@gmail.com',
    apiKey: 'ff5f107764b2f2bb57a4715ea6b95f9a978a39ebcae6d53e68692e642fc1fc75',
    endpointDomain: 'https://api.unsplash.com/',
    requestBase: 'photos/random/',
    imagesLimit: 5
}

// Where to save images
var path = './saved_images/';

// Images filename (will append progressive number)
var filenameBase = 'image-';

// Function used to get image from URL and save it
function saveImageFromUrl(imageUrl, folder, filename) {
    wget(
        {
            url: imageUrl,
            dest: folder + filename
        },
        function (error) {
            if (error)
                console.log('Could not export image: ' + error);
            else
                console.log('Image exported.');
        }
    );
}

// REST request to get random images from Unsplash
axios
    .get(
        req.endpointDomain + req.requestBase,
        {
            params: {
                client_id: req.apiKey,
                count: req.imagesLimit
            }
        }
    )
    .then(
        function (response) {
            for (var i = 0; i < req.imagesLimit; i++) {
                var imageUrl = response.data[i].urls.full;
                var imageFilename = filenameBase + (i + 1).toString() + '.jpg';
                saveImageFromUrl( imageUrl, path, imageFilename );
            }
        }
    )
    .catch(
        function (error) {
            if (error)
                console.log(error);
        }
    );
